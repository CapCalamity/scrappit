﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappit
{
    public static class Extensions
    {
        public static string Left(this string @this, int chars)
        {
            if (@this.Length > chars)
            {
                return @this.Substring(0, chars);
            }
            else
            {
                return @this;
            }
        }

        public static string Right(this string @this, int chars)
        {
            if (@this.Length > chars)
            {
                return @this.Substring(@this.Length - chars, chars);
            }
            else
            {
                return @this;
            }
        }

        public static string Replace(this string @this, IEnumerable<char> characters, char replacement)
        {
            var tmp = @this;
            foreach (char c in characters)
            {
                tmp = tmp.Replace(c, replacement);
            }
            return tmp;
        }
    }
}
