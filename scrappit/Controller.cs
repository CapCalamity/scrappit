﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedditSharp;
using RedditSharp.Things;
using System.Threading;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Scrappit
{
    class Controller
    {
        private const string HistoryFile = "./history.dat";

        private Options _options;
        private Reddit _baseReddit;
        private List<SubParser> _parser;
        private PostHistory _history;
        private DownloadQueue _queue;
        private Thread _dlThread;
        private bool _runDownloads;
        private object _downloadLock;

        public Controller(Options options)
        {
            _options = options;
            _baseReddit = new Reddit(WebAgent.RateLimitMode.Pace);
            _parser = new List<SubParser>();
            _queue = new DownloadQueue();
            _dlThread = new Thread(DownloadFiles);
            _runDownloads = false;
            _downloadLock = new object();

            LoadHistory();

            Initialize();
        }

        private void Initialize()
        {
            ImgurResolver.DownloadGifs = _options.Gifs;

            foreach (var sub in _options.Subreddits)
            {
                _parser.Add(new SubParser(
                    sub,
                    _baseReddit,
                    _options,
                    (post) => { _history.Add(post.Id); },
                    (post) => { return !_history.Contains(post.Id); }));
            }
        }

        private void LoadHistory()
        {
            try
            {
                if (File.Exists(HistoryFile))
                {
                    _history = JsonConvert.DeserializeObject<PostHistory>(
                        File.ReadAllText(HistoryFile),
                        new JsonSerializerSettings
                        {
                            ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver
                            {
                                DefaultMembersSearchFlags = System.Reflection.BindingFlags.NonPublic
                                | System.Reflection.BindingFlags.Public
                                | System.Reflection.BindingFlags.Static
                            }
                        });
                }
                else
                {
                    _history = new PostHistory();
                }
            }
            catch (Exception)
            {
                _history = new PostHistory();
            }
        }

        private void SaveHistory()
        {
            File.WriteAllText(HistoryFile, JsonConvert.SerializeObject(
                _history,
                new JsonSerializerSettings
                {
                    ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver
                    {
                        DefaultMembersSearchFlags = System.Reflection.BindingFlags.NonPublic
                        | System.Reflection.BindingFlags.Public
                        | System.Reflection.BindingFlags.Static
                    }
                }));
        }

        public void Start()
        {
            lock (_downloadLock)
            {
                _runDownloads = true;
            }

            _dlThread.Start();

            var finished = new List<bool>();
            do
            {
                finished.Clear();
                foreach (var sub in _parser)
                {
                    try
                    {
                        foreach (var file in sub.ResolvePost())
                        {
                            _queue.Enqueue(file);
                            Console.WriteLine("+ | {0}", file.Name.Right(70));
                            Thread.Sleep(2000);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                    finally
                    {
                        finished.Add(sub.Finished);
                    }
                }
            }
            while (!finished.All(fin => fin));

            while (!_queue.IsEmpty)
                Thread.Sleep(1000);

            lock (_downloadLock)
            {
                _runDownloads = false;
            }

            SaveHistory();

            PrintStats();

            Console.WriteLine("program finished, press enter to close");
            Console.ReadLine();
        }

        private void PrintStats()
        {
            Console.WriteLine();

            foreach (var sub in _parser)
            {
                Console.WriteLine("{0}:", sub.Name);
                Console.WriteLine("Posts Parsed:         {0}", sub.PostsParsed);
                Console.WriteLine("Posts Resolved:       {0}", sub.PostsResolved);
                Console.WriteLine("Posts Skipped:        {0}", sub.PostsSkipped);
                Console.WriteLine();
            }

            Console.WriteLine("Total:");
            Console.WriteLine("Total Posts Parsed:   {0}", _parser.Select(sub => sub.PostsParsed).Sum());
            Console.WriteLine("Total Posts Resolved: {0}", _parser.Select(sub => sub.PostsResolved).Sum());
            Console.WriteLine("Total Posts Skipped:  {0}", _parser.Select(sub => sub.PostsSkipped).Sum());

            Console.WriteLine();

            Console.WriteLine("Imgur Tokens:");

            Console.WriteLine("Application Tokens:   {0}/{1}",
                ImgurResolver.TokensRemaining,
                ImgurResolver.TokenLimit);

            Console.WriteLine("User Tokens:          {0}/{1}",
                ImgurResolver.UserRemaining,
                ImgurResolver.UserLimit);

            var now = DateTime.Now;
            var then = (new DateTime(now.Year, now.Month, now.Day, 0, 0, 0).AddDays(1)) - now;

            Console.WriteLine("Token-Reset App:      {0}",
                new TimeSpan(then.Ticks));

            Console.WriteLine("Token-Reset User:     {0}",
                new TimeSpan(new DateTime(ImgurResolver.UserReset).Ticks));
        }

        private void DownloadFiles()
        {
            var client = new WebClient();
            while (_runDownloads)
            {
                ResolvedFile file;
                if (!_queue.IsEmpty && _queue.TryGetNext(out file))
                {
                    try
                    {
                        var finfo = new FileInfo(_options.BaseSaveFolder + "\\" + file.Name);
                        if (!Directory.Exists(finfo.DirectoryName))
                            Directory.CreateDirectory(finfo.DirectoryName);

                        Console.WriteLine("> | {0}", finfo.FullName.Right(70));

                        client.DownloadFile(file.Link, finfo.FullName);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                }
            }
        }
    }
}
