﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappit
{
    class ResolvedFile
    {
        public string Name { get; set; }
        public Uri Link { get; set; }
        public DateTime CreatedOn { get; set; }
        public event Action<ResolvedFile> OnSuccess;

        public ResolvedFile()
        {
            CreatedOn = DateTime.Now;
        }

        public ResolvedFile(string name, Uri link)
            : this()
        {
            Name = name;
            Link = link;
        }

        public ResolvedFile(string name, string link)
            : this()
        {
            Name = name;
            Link = new Uri(link);
        }

        public ResolvedFile(string name, Uri link, DateTime createdOn)
        {
            Name = name;
            Link = link;
            CreatedOn = createdOn;
        }

        public ResolvedFile(string name, string link, DateTime createdOn)
        {
            Name = name;
            Link = new Uri(link);
            CreatedOn = createdOn;
        }

        public void TriggerSuccess()
        {
            if (OnSuccess != null)
            {
                OnSuccess(this);
            }
        }
    }
}
