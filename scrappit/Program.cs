﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace Scrappit
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();

            var parser = new CommandLine.Parser((conf) =>
            {
                conf.IgnoreUnknownArguments = false;
                conf.CaseSensitive = false;
            });

            try
            {
                if (parser.ParseArguments(args, options))
                {
                    if (options.ShowHelp)
                    {
                        Console.WriteLine(options.GetUsage());
                        return;
                    }

                    new Controller(options).Start();
                }
                else
                {
                    Console.WriteLine(options.GetUsage());
                    return;
                }
            }
            catch (Exception e)
            {
                if (e is System.Reflection.CustomAttributeFormatException || e is ParserException)
                    Console.WriteLine(options.GetUsage());
                else
                    throw;
            }
        }
    }
}
