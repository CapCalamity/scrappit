﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace Scrappit
{
    class Options
    {
        [Option('h', "help",
            Required = false,
            DefaultValue = false,
            HelpText = "Show this message")]
        public bool ShowHelp { get; set; }

        [Option('p', "posts",
            Required = true,
            HelpText = "Number of posts to parse for each given subreddit",
            MutuallyExclusiveSet = "help")]
        public int PostsToTake { get; set; }

        [OptionList('s', "subreddits",
            Required = true,
            Separator = ',',
            HelpText = "List of subreddit names",
            MutuallyExclusiveSet = "help")]
        public List<string> Subreddits { get; set; }

        [Option('b', "base",
            Required = false,
            DefaultValue = "./",
            HelpText = "Base folder where files will be stored in",
            MutuallyExclusiveSet = "help")]
        public string BaseSaveFolder { get; set; }

        [Option('f', "f",
            Required = false,
            DefaultValue = "{sub}\\{name}.{type}",
            HelpText = "Pattern determining the resulting folder/filename of any resulting file",
            MutuallyExclusiveSet = "help")]
        public string SavePattern { get; set; }

        [Option('x', "nsfw",
            Required = false,
            DefaultValue = false,
            HelpText = "parse nsfw tagged posts",
            MutuallyExclusiveSet = "help")]
        public bool Nsfw { get; set; }

        [Option('g', "gifs",
            Required = false,
            DefaultValue = false,
            HelpText = "download gifs (depending on length, gifs may be up to 100mB in size)",
            MutuallyExclusiveSet = "help")]
        public bool Gifs { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
