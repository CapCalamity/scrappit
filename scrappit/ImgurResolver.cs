﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Scrappit
{
    class ImgurResolver : IResolver
    {
        private const string ApiUrl = "https://api.imgur.com/3/";
        private const string ClientId = "de388ca6d25724f";
        private const string ClientSecret = "cd140d85e2c0e897e3828d61896c2221ac0800c1";

        public static bool DownloadGifs { get; set; }

        public static int TokenLimit { get; private set; }
        public static int TokensRemaining { get; private set; }
        public static int UserLimit { get; private set; }
        public static int UserRemaining { get; private set; }
        public static long UserReset { get; private set; }

        private readonly Uri _baseUri;

        public ImgurResolver(Uri uri)
        {
            _baseUri = uri;
        }

        private string DownloadFrom(Uri url)
        {
            try
            {
                var client = new WebClient
                {
                    Headers = new WebHeaderCollection
                    {
                        "Authorization: Client-ID " + ClientId
                    }
                };
                //client.Headers.Add("Authorization", "Client-ID " + ClientId);

                var raw = client.DownloadString(url);

                TokensRemaining = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-ClientRemaining"]);
                TokenLimit = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-ClientLimit"]);
                UserLimit = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-UserLimit"]);
                UserRemaining = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-UserRemaining"]);
                UserRemaining = Convert.ToInt32(client.ResponseHeaders["X-RateLimit-UserRemaining"]);
                UserReset = Convert.ToInt64(client.ResponseHeaders["X-RateLimit-UserReset"]);

                return raw;
            }
            catch (WebException) { return ""; }
        }

        private IEnumerable<string> ResolveAlbum()
        {
            var items = new List<string>();

            try
            {
                string albumID = _baseUri.AbsolutePath.StartsWith("/a/") ? _baseUri.AbsolutePath.Substring("/a/".Length) : String.Empty;
                if (albumID == String.Empty)
                    return new String[] { };

                string api = ApiUrl + "album/" + albumID;
                if (api.EndsWith("/"))
                    api = api.TrimEnd('/');

                string raw = DownloadFrom(new Uri(api));

                if (raw == "")
                    return items;

                dynamic json = JsonConvert.DeserializeObject(raw);

                foreach (dynamic img in json["data"]["images"])
                    items.Add(img["link"].Value);
            }
            catch (Exception)
            {
                items.Clear();
            }

            return items;
        }

        private string ResolveImage()
        {
            string retVal = String.Empty;
            try
            {
                string api = ApiUrl + "image/" + _baseUri.AbsolutePath.TrimEnd('/').Split('/').Last();
                if (api.EndsWith("/"))
                    api = api.TrimEnd('/');

                string raw = DownloadFrom(new Uri(api));

                if (raw == "")
                    return retVal;

                dynamic json = JsonConvert.DeserializeObject(raw);

                retVal = json["data"]["link"].Value;
            }
            catch (Exception)
            {
                retVal = String.Empty;
            }

            return retVal;
        }

        public IEnumerable<string> ResolveURI()
        {

            if (new[] { ".jpg", ".jpeg", ".bmp", ".png" }.Any(t => _baseUri.AbsolutePath.EndsWith(t))
                || (DownloadGifs && _baseUri.AbsolutePath.EndsWith(".gif")))
            {
                return new[] { _baseUri.AbsoluteUri };
            }

            if (DownloadGifs && _baseUri.AbsolutePath.EndsWith("gifv"))
            {
                return new[] { _baseUri.AbsoluteUri.Substring(0, _baseUri.AbsoluteUri.Length - 1) };
            }

            //if it is an album
            if (_baseUri.AbsoluteUri.Contains("/a/"))
            {
                return ResolveAlbum();
            }

            return new[] { ResolveImage() };
        }
    }
}
