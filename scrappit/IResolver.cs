﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappit
{
    interface IResolver
    {
        IEnumerable<string> ResolveURI();
    }
}
