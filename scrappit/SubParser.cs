﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using RedditSharp;
using RedditSharp.Things;

namespace Scrappit
{
    class SubParser
    {
        public bool Finished { get; private set; }
        public int PostsParsed { get; private set; }
        public int PostsResolved { get; private set; }
        public int PostsSkipped { get; private set; }
        public string Name
        {
            get
            {
                return _sub.DisplayName;
            }
        }

        private Subreddit _sub;
        private Action<Post> _onPostResolved;
        private Func<Post, bool> _decideDownload;
        private Options _options;

        private SubParser()
        {
            Finished = false;
            PostsParsed = 0;
            PostsResolved = 0;
            PostsSkipped = 0;
        }

        public SubParser(string subName, Reddit baseReddit, Options options, Action<Post> onPostResolved, Func<Post, bool> decideDownload)
            : this()
        {
            _sub = baseReddit.GetSubreddit(subName);
            _options = options;
            _onPostResolved = onPostResolved;
            _decideDownload = decideDownload;
        }

        public SubParser(Subreddit sub, Options options, Action<Post> onPostResolved, Func<Post, bool> decideDownload)
            : this()
        {
            _sub = sub;
            _options = options;
            _onPostResolved = onPostResolved;
            _decideDownload = decideDownload;
        }

        public IEnumerable<ResolvedFile> ResolvePost()
        {
            foreach (var post in _sub.Hot.Take(_options.PostsToTake))
            {
                ++PostsParsed;
                //decide whether or not post will be skipped
                if (!_decideDownload(post)
                    || (post.NSFW && !_options.Nsfw))
                {
                    ++PostsSkipped;
                    continue;
                }

                var resolved = Resolve(post);

                ++PostsResolved;
                _onPostResolved(post);

                foreach (var file in resolved)
                {
                    yield return file;
                }
            }

            Finished = true;

            yield break;
        }

        private IEnumerable<ResolvedFile> Resolve(Post p)
        {
            var links = new List<string>();

            if (p.Url.Host.Contains("imgur.com"))
            {
                links.AddRange(new ImgurResolver(p.Url)
                    .ResolveURI()
                    .Where(link => link != ""));
            }
            else
            {
                return new ResolvedFile[] { };
            }

            var files = new List<ResolvedFile>(links.Count);

            int index = 0;
            foreach (var link in links)
            {
                files.Add(new ResolvedFile(GetNameFor(p, link, links.Count > 1, index++), link));
            }

            return files;
        }

        private string GetNameFor(Post p, string link, bool isAlbum = false, int albumIndex = 0)
        {
            var type = "";
            var name = _options.SavePattern;

            if (isAlbum)
            {
                name = name.Replace("{name}", "{name}\\{index}");
            }

            var match = Regex.Match(link, @"(.+\.)([\w\d]+)([?/\\&\w\d=\[\]]*)");
            if (match.Success)
            {
                type = match.Groups[2].Value;
            }

            name = name.Replace("{sub}", p.Subreddit)
                .Replace("{name}", p.Title.Left(50).Replace(Path.GetInvalidFileNameChars(), '_'))
                .Replace("{index}", albumIndex.ToString())
                .Replace("{type}", type);

            return name;
        }
    }
}
