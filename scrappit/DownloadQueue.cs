﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappit
{
    class DownloadQueue
    {
        private ConcurrentQueue<ResolvedFile> _queue;

        public bool IsEmpty
        {
            get
            {
                lock (_queue)
                {
                    return _queue.IsEmpty;
                }
            }
        }

        public DownloadQueue()
        {
            _queue = new ConcurrentQueue<ResolvedFile>();
        }

        public bool TryGetNext(out ResolvedFile file)
        {
            file = null;
            try
            {
                if (_queue.IsEmpty)
                {
                    return false;
                }
                else
                {
                    return _queue.TryDequeue(out file);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return false;
            }
        }

        public void Enqueue(ResolvedFile file)
        {
            _queue.Enqueue(file);
        }
    }
}
