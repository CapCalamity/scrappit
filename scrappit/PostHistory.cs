﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Scrappit
{
    class PostHistory
    {
        [JsonProperty("history")]
        private HashSet<string> _resolvedPostIds;

        public PostHistory()
        {
            _resolvedPostIds = new HashSet<string>();
        }

        public void Add(string id)
        {
            _resolvedPostIds.Add(id);
        }

        public bool Contains(string id)
        {
            return _resolvedPostIds.Contains(id);
        }
    }
}
